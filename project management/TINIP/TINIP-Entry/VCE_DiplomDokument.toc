\babel@toc {ngerman}{}
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{8}{chapter*.6}%
\contentsline {chapter}{\numberline {1}Einleitung}{9}{chapter.1}%
\contentsline {chapter}{\numberline {2}Begriffsbestimmungen}{10}{chapter.2}%
\contentsline {chapter}{\numberline {3}Metainformationen}{12}{chapter.3}%
\contentsline {section}{\numberline {3.1}Projektteam}{12}{section.3.1}%
\contentsline {section}{\numberline {3.2}Projektbetreuer}{12}{section.3.2}%
\contentsline {section}{\numberline {3.3}Projektpartner}{12}{section.3.3}%
\contentsline {chapter}{\numberline {4}Projekt Management}{13}{chapter.4}%
\contentsline {section}{\numberline {4.1}Agile}{13}{section.4.1}%
\contentsline {section}{\numberline {4.2}Ist-Zustand}{14}{section.4.2}%
\contentsline {section}{\numberline {4.3}Soll-Zustand}{14}{section.4.3}%
\contentsline {chapter}{\numberline {5}Vor-Entwurf}{15}{chapter.5}%
\contentsline {chapter}{\numberline {6}Rapid Prototyping (Experimental Prototyping)}{16}{chapter.6}%
\contentsline {section}{\numberline {6.1}Prototyp 1 - Parsec}{16}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Beschreibung}{16}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Research}{17}{subsection.6.1.2}%
\contentsline {subsection}{\numberline {6.1.3}Wissensgewinn}{17}{subsection.6.1.3}%
\contentsline {section}{\numberline {6.2}Prototyp 2 - Unity DOTS, Web Sockets}{17}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Beschreibung}{17}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Meilensteine}{17}{subsection.6.2.2}%
\contentsline {subsection}{\numberline {6.2.3}Research}{18}{subsection.6.2.3}%
\contentsline {subsection}{\numberline {6.2.4}Einstellung des DOTS Prototypen}{19}{subsection.6.2.4}%
\contentsline {subsection}{\numberline {6.2.5}Wissengewinnung}{19}{subsection.6.2.5}%
\contentsline {chapter}{\numberline {7}Das finale Produkt - inkrementeller Prototyp}{20}{chapter.7}%
\contentsline {section}{\numberline {7.1}Einleitung}{20}{section.7.1}%
\contentsline {section}{\numberline {7.2}Entwurf}{21}{section.7.2}%
\contentsline {section}{\numberline {7.3}Technische Umsetzung}{22}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}Aufsetzen des Projekts}{22}{subsection.7.3.1}%
\contentsline {subsection}{\numberline {7.3.2}Codeausschnitte - Logik der Applikation}{26}{subsection.7.3.2}%
\contentsline {subsection}{\numberline {7.3.3}Source Code - Gitlab Repository}{32}{subsection.7.3.3}%
\contentsline {section}{\numberline {7.4}Showcase Work-in-Progress}{33}{section.7.4}%
\contentsline {section}{\numberline {7.5}Geplant ist noch}{34}{section.7.5}%
\contentsline {chapter}{\numberline {8}Sonstige Eingesetzte Technologien}{35}{chapter.8}%
\contentsline {chapter}{\numberline {9}Fazit}{36}{chapter.9}%
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{37}{chapter*.23}%
\contentsline {chapter}{\numberline {A}Anhang}{41}{appendix.A}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
