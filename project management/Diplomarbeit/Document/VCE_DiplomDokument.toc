\babel@toc {ngerman}{}
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{9}{chapter*.6}%
\contentsline {chapter}{\numberline {1}Begriffsbestimmungen}{10}{chapter.1}%
\contentsline {chapter}{\numberline {2}Einleitung}{12}{chapter.2}%
\contentsline {chapter}{\numberline {3}Projektmanagement}{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}Metainformationen}{13}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Projektteam}{13}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Projektbetreuer}{13}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Projektpartner}{13}{subsection.3.1.3}%
\contentsline {section}{\numberline {3.2}Agiler Projektablauf}{14}{section.3.2}%
\contentsline {section}{\numberline {3.3}Problemstellung}{15}{section.3.3}%
\contentsline {section}{\numberline {3.4}Lösungsansatz}{16}{section.3.4}%
\contentsline {section}{\numberline {3.5}Projektumfeldanalyse}{17}{section.3.5}%
\contentsline {section}{\numberline {3.6}Risikoanalyse}{19}{section.3.6}%
\contentsline {section}{\numberline {3.7}Risikomanagement}{20}{section.3.7}%
\contentsline {section}{\numberline {3.8}Pflichtenheft}{21}{section.3.8}%
\contentsline {subsection}{\numberline {3.8.1}Zielsetzung}{21}{subsection.3.8.1}%
\contentsline {subsection}{\numberline {3.8.2}Produkteinsatz}{21}{subsection.3.8.2}%
\contentsline {subsection}{\numberline {3.8.3}Funktionalitäten}{22}{subsection.3.8.3}%
\contentsline {subsubsection}{\nonumberline Funktionale Anforderungen}{22}{subsubsection*.11}%
\contentsline {subsubsection}{\nonumberline Nicht-funktionale Anforderungen}{22}{subsubsection*.13}%
\contentsline {subsection}{\numberline {3.8.4}Testszenarien und Testfälle}{23}{subsection.3.8.4}%
\contentsline {subsection}{\numberline {3.8.5}Abnahmekriterien}{23}{subsection.3.8.5}%
\contentsline {chapter}{\numberline {4}Vorentwurf}{24}{chapter.4}%
\contentsline {section}{\numberline {4.1}Warum Unity DOTS?}{25}{section.4.1}%
\contentsline {chapter}{\numberline {5}Rapid Prototyping - experimentelle Prototypen}{26}{chapter.5}%
\contentsline {section}{\numberline {5.1}Prototyp 1 - Parsec}{26}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Beschreibung}{26}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Research}{27}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Einstellung des Prototypen}{27}{subsection.5.1.3}%
\contentsline {subsection}{\numberline {5.1.4}Backlog}{27}{subsection.5.1.4}%
\contentsline {section}{\numberline {5.2}Prototyp 2 - Unity DOTS, Web Sockets}{28}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Beschreibung}{28}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Meilensteine}{28}{subsection.5.2.2}%
\contentsline {subsection}{\numberline {5.2.3}P2 Research}{28}{subsection.5.2.3}%
\contentsline {subsection}{\numberline {5.2.4}Einstellung des DOTS Prototypen}{31}{subsection.5.2.4}%
\contentsline {subsection}{\numberline {5.2.5}Wissengewinnung}{31}{subsection.5.2.5}%
\contentsline {chapter}{\numberline {6}Das finale Produkt - inkrementeller Prototyp}{32}{chapter.6}%
\contentsline {section}{\numberline {6.1}Einleitung}{32}{section.6.1}%
\contentsline {section}{\numberline {6.2}Entwurf}{33}{section.6.2}%
\contentsline {section}{\numberline {6.3}Technische Umsetzung}{34}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Aufsetzen des Projekts}{34}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}Codeausschnitte - Logik der Applikation}{39}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}Source Code - Gitlab Repository}{45}{subsection.6.3.3}%
\contentsline {section}{\numberline {6.4}Showcase Applikation}{46}{section.6.4}%
\contentsline {chapter}{\numberline {7}Testungen}{48}{chapter.7}%
\contentsline {section}{\numberline {7.1}Testungsverfahren}{48}{section.7.1}%
\contentsline {section}{\numberline {7.2}Testergebnisse}{50}{section.7.2}%
\contentsline {section}{\numberline {7.3}Mögliche Verbesserungen des Produkts nach der Diplomarbeit}{51}{section.7.3}%
\contentsline {chapter}{\numberline {8}Evaluation}{52}{chapter.8}%
\contentsline {section}{\numberline {8.1}Projektevaluation}{52}{section.8.1}%
\contentsline {section}{\numberline {8.2}Produktevaluation}{53}{section.8.2}%
\contentsline {section}{\numberline {8.3}Resümee}{53}{section.8.3}%
\contentsline {chapter}{\numberline {9}Zusammenfassung}{54}{chapter.9}%
\contentsline {chapter}{\numberline {10}Präsentationsvideo Visual Coworking Environment}{56}{chapter.10}%
\contentsline {chapter}{\numberline {11}Sonstige eingesetzte Arbeitsmittel und Tools}{57}{chapter.11}%
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{58}{chapter*.30}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
