# Visual-Coworking-Environment
Our Diploma Project for IT-College in Imst

## Release
[1.0: Diploma Project finished and handed in](https://gitlab.com/Rushhourzet/Visual-Coworking-Environment/-/releases/1.0)

## Abstract
The aim of this thesis is the implementation of a prototype for a chat platform that should facilitate collaboration. We would like to set the foundation for a product that can develop into a VR pioneer product. Thus, this project is built with the idea of expandability and performance in mind. In order to achieve this, however, we decide against using object-oriented programming (OOP) and instead using “Data-Oriented design” (DoD) and an “Entity Component System” (ECS). We agreed to this architecture as the basis for this project because of performance, flexibility and easy-to-read code.
## Full Thesis
[Diploma Thesis](https://gitlab.com/Rushhourzet/Visual-Coworking-Environment/-/blob/development/project%20management/Diplomarbeit/Document/VCE_DiplomDokument.pdf)
## Screenshots
### This first screenshot shows the main menu of our application. In the center there is the network panel in which you can type in your name and connect with other clients.
![screenshot-1](https://gitlab.com/Rushhourzet/Visual-Coworking-Environment/-/raw/development/project%20management/Diplomarbeit/Document/014_showcase.png)
### The second screenshot shows two clients connected to each other. It is possible to move around and talk.
![screenshot-2](https://gitlab.com/Rushhourzet/Visual-Coworking-Environment/-/raw/development/project%20management/Diplomarbeit/Document/015_wipapplication.png)
