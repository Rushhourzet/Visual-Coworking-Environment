﻿using UnityEngine;

namespace VCE.Core {
    [CreateAssetMenu(fileName = "User Config", menuName = "ScriptableObjects/User/Config")]
    public class UserConfig : ScriptableObject {
        public float sens;
    }
}
