﻿using System;
using UnityEngine;
using Unity.Mathematics;

namespace VCE.Core {
    public struct PlayerMovementComponent {
        public Vector3 PositionMovement;
        public Vector3 RotationMovement;
    }
}
