﻿using System;
using UnityEngine;
using Unity.Mathematics;
using MLAPI;

#if false
//Once we move Player from AoS to SoA, we can network arrays of Positions and Rotations and move them all here which will increase performance #optimizationBois
namespace VCE.Core {
    public class PlayerMovementSystem : MonoBehaviour {
        public PlayerInputSystem inputSystem;
        private Player localPlayer;
        private void Start() {
            if (NetworkManager.Singleton.ConnectedClients.TryGetValue(NetworkManager.Singleton.LocalClientId,
                    out var networkedClient)) {
                localPlayer = networkedClient.PlayerObject.GetComponent<Player>();

            }
        }
        private void Update() {
            if (localPlayer) {
                localPlayer.InputHandling(inputSystem);
            }
        }

    }
} 
#endif
