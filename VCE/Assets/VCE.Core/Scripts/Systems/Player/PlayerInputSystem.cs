﻿using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;
using Unity.Mathematics;
using System;
using MLAPI.Serialization;

namespace VCE.Core {
    public class PlayerInputSystem : MonoBehaviour, INetworkSerializable {
        public PlayerMovementComponent movement;
        #region Start - Initialize movement
        private void Start() {
            movement = new PlayerMovementComponent();
        }
        #endregion

        #region Update - Reads Player Input and converts it to movement vectors
        private void Update() {
            if (Cursor.lockState == CursorLockMode.Locked) {
                PlayerMovementFromInput(ref movement);
            }
        }

        public static void PlayerMovementFromInput(ref PlayerMovementComponent movement) {
            float pz = Input.GetAxis("Vertical");
            float px = Input.GetAxis("Horizontal");
            float ry = Input.GetAxis("Mouse X");
            float rx = Input.GetAxis("Mouse Y")*-1; // *-1 gets rid of Inversion

            movement.PositionMovement = new float3(px, 0f, pz);
            movement.RotationMovement = new float3(rx, ry, 0f);
        }

        public void NetworkSerialize(NetworkSerializer serializer) {
            serializer.Serialize(ref movement.PositionMovement);
            serializer.Serialize(ref movement.RotationMovement);
        }
        #endregion
    }
}
