﻿using System;
using UnityEngine;
using MLAPI;

namespace VCE.Core {
    public class LocalUserSystem : MonoBehaviour, IMLAPIConnectionEventTarget{
        public UserData userData;
        public UserConfig userConfig;
        public PlayerInputSystem inputSystem;
        public Player player;
        private Player localPlayer;
        public GameManager gameMan;

        private void Start() {
            //localPlayer = player.GetLocalPlayer();
        }

        #region ConnectionEventHandling
        public void ClientConnectedEvent(string username) {
            userData.username = username;
        }
        public void HostedEvent(string username) {
            userData.username = username;
        }
        public void ServerHostedEvent() {
            //do nothing
        } 
        #endregion
    }
}
