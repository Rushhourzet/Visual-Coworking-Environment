﻿//research: https://answers.unity.com/questions/869378/viewing-desktop-in-scene.html
//http://forum.unity3d.com/threads/windows-api-calls.127719/
//https://www.youtube.com/watch?v=RqgsGaMPZTw

using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices; 

public class TransparentWindow : MonoBehaviour {
#if !UNITY_EDITOR     //Only runs when not in editor, though it only works on windows right now
    #region MARGINS struct
		    private struct MARGINS
        {
            public int cxLeftWidth;
            public int cxRightWidth;
            public int cyTopHeight;
            public int cyBottomHeight;
        } 
    #endregion

    #region dll Imports
        [DllImport("user32.dll")]
        private static extern IntPtr GetActiveWindow();
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, uint dwNewLong);
        [DllImport("user32.dll")]
        static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll", EntryPoint = "SetLayeredWindowAttributes")]
        static extern int SetLayeredWindowAttributes(IntPtr hwnd, int crKey, byte bAlpha, int dwFlags);
        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        private static extern int SetWindowPos(IntPtr hwnd, int hwndInsertAfter, int x, int y, int cx, int cy, int uFlags);
        [DllImport("Dwmapi.dll")]
        private static extern uint DwmExtendFrameIntoClientArea(IntPtr hWnd, ref MARGINS margins); 
    #endregion

    #region Windows Constants
        //no magic numbers allowed, those are taken from https://docs.microsoft.com/en-us/windows/win32/winmsg/constants
        const int GWL_STYLE = -16;
        const uint WS_POPUP = 0x80000000;
        const uint WS_VISIBLE = 0x10000000;
        const int WS_EX_TOPMOST = 0x00000008;
        const int WS_EX_LAYERED = 0x00080000;
        const int WS_EX_TRANSPARENT = 0x00000020;
        const int GWL_EXSTYLE = -20;
        const int HWND_TOPMOST = -1;
        const int LWA_COLORKEY = 0x00000001;
        const int LWA_ALPHA = 0x00000002;
    #endregion

    IntPtr hwnd; //Variable for the current window

    #region Start - makes background transparent and clicktrough
		        void Start() {
            hwnd = GetActiveWindow();

            int fWidth = Screen.width;
            int fHeight = Screen.height;

            var margins = new MARGINS() { cxLeftWidth = -1 };
            DwmExtendFrameIntoClientArea(hwnd, ref margins);

            // SetWindowLong(hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
            // Transparent windows with click through
            SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);
            SetLayeredWindowAttributes(hwnd, 0, 0, LWA_COLORKEY);//makes background transparent
            //SetLayeredWindowAttributes(hwnd, 0, 0, WS_EX_TOPMOST);
            SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, 0); //SWP_FRAMECHANGED = 0x0020 (32); //SWP_SHOWWINDOW = 0x0040 (64)
        }
        private void Update() {
            //SetClickthrough(Physics2D.OverlapPoint(Input.mousePosition) == null);
        }

        private void SetClickthrough(bool clickthrough) {
            if (clickthrough) {
                SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED | WS_EX_TRANSPARENT);
            }
            else {
                SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);
            }
        }
    #endregion

#endif
}
