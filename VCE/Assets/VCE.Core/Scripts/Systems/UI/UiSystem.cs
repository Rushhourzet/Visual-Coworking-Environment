﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace VCE.Core {
    public class UiSystem : MonoBehaviour, INetworkPanelVisibilityTarget {
        #region Variables - References
        public GameObject networkPanel;
        public InputField nameInputField;
        public InputField addressInputField;
        public InputField portInputField;
        public Camera ServerCamera;
        #endregion        
        #region Variables - UnityActions
        public UnityAction showNetworkPanelAction { private set; get; }
        public UnityAction hideNetworkPanelAction { private set; get; }
        #endregion

        #region Start - Show NetworkPanel, init Actions
        private void Start() {
            ShowNetworkPanel(true);
            showNetworkPanelAction = ShowNetworkPanel;
            hideNetworkPanelAction = HideNetworkPanel;
        }
        #endregion

        #region Show/Hide Networkpanel
        public void ShowNetworkPanel(bool show) {
            networkPanel.SetActive(show);
            var inputFields = networkPanel.GetComponentsInChildren<InputField>();
            foreach (var f in inputFields) {
                f.interactable = show;
            }
            ServerCamera.gameObject.SetActive(show);
        }
        public void ShowNetworkPanel() {
            networkPanel.SetActive(true);
            var inputFields = networkPanel.GetComponentsInChildren<InputField>();
            foreach (var f in inputFields) {
                f.interactable = true;
            }
            ServerCamera.gameObject.SetActive(true);
        }
        public void HideNetworkPanel() {
            networkPanel.SetActive(false);
            var inputFields = networkPanel.GetComponentsInChildren<InputField>();
            foreach (var f in inputFields) {
                f.interactable = false;
            }
            ServerCamera.gameObject.SetActive(false);
        }

        #endregion

        #region GetUsername from Inputfield
        public string GetUsername() {
            return nameInputField.text.Replace(" ", "_");
        } 
        public string GetAddress() {
            return addressInputField.text;
        }
        public ushort GetPort() {
            return UInt16.Parse(portInputField.text);
        }
        #endregion
    }
}
