using UnityEngine;
using MLAPI;
using UnityEngine.EventSystems;
using MLAPI.Transports.UNET;

namespace VCE.Core {
    public class ConnectionSystem : MonoBehaviour {
        public UiSystem uiSystem;
        #region Variables - GameObject References
        public GameObject spawn_go;
        public GameObject vivoxLogin_go;
        public GameObject localUser_go;
        public UNetTransport transport;
        private GameObject uiSystem_go => uiSystem.gameObject;
        #endregion
        #region Connect
        public void Client() {
            DisconnectFromWhateverYouAre();
            AssignAddressAndPortFromInputFields();
            NetworkManager.Singleton.ConnectionApprovalCallback += ApprovalCheck;
            NetworkManager.Singleton.StartClient();
            var username = uiSystem.GetUsername();
            ClientConnectedEvent(username);
        }
        public void Host() {
            DisconnectFromWhateverYouAre();
            AssignAddressAndPortFromInputFields();
            NetworkManager.Singleton.ConnectionApprovalCallback += ApprovalCheck;
            NetworkManager.Singleton.StartHost();
            var username = uiSystem.GetUsername();
            HostedEvent(username);
        }
        public void Server() {
            DisconnectFromWhateverYouAre();
            AssignAddressAndPortFromInputFields();
            NetworkManager.Singleton.StartServer();
            if (NetworkManager.Singleton.IsHost) {
                ServerHostedEvent();
            }
        } 
        #endregion
        #region Disconnect
        private void DisconnectFromWhateverYouAre() {
            if (NetworkManager.Singleton.IsConnectedClient) {
                NetworkManager.Singleton.StopClient();
            }
            if (NetworkManager.Singleton.IsHost) {
                NetworkManager.Singleton.StopHost();
            }
            if (NetworkManager.Singleton.IsServer) {
                NetworkManager.Singleton.StopHost();
            }
        } 
        #endregion

        #region ConnectionEvents
        public void ClientConnectedEvent(string username) {
            ExecuteEvents.Execute<IMLAPIConnectionEventTarget>(localUser_go, null, (x, y) => x.ClientConnectedEvent(username));
            ExecuteEvents.Execute<IMLAPIConnectionEventTarget>(vivoxLogin_go, null, (x, y) => x.ClientConnectedEvent(username));
            ExecuteEvents.Execute<INetworkPanelVisibilityTarget>(uiSystem_go, null, (x, y) => x.HideNetworkPanel());
        }

        public void HostedEvent(string username) {
            ExecuteEvents.Execute<IMLAPIConnectionEventTarget>(localUser_go, null, (x, y) => x.HostedEvent(username));
            ExecuteEvents.Execute<IMLAPIConnectionEventTarget>(vivoxLogin_go, null, (x, y) => x.HostedEvent(username));
            ExecuteEvents.Execute<INetworkPanelVisibilityTarget>(uiSystem_go, null, (x, y) => x.HideNetworkPanel());
        }

        public void ServerHostedEvent() {
            ExecuteEvents.Execute<IMLAPIConnectionEventTarget>(localUser_go, null, (x, y) => x.ServerHostedEvent());
            ExecuteEvents.Execute<IMLAPIConnectionEventTarget>(vivoxLogin_go, null, (x, y) => x.ServerHostedEvent());
            ExecuteEvents.Execute<INetworkPanelVisibilityTarget>(uiSystem_go, null, (x, y) => x.HideNetworkPanel());
        }
        #endregion

        private void AssignAddressAndPortFromInputFields() {
            int port = uiSystem.GetPort();
            transport.ServerListenPort = port;
            transport.ConnectAddress = uiSystem.GetAddress();
            transport.ConnectPort = port;
        }

        #region Approvalcheck - Makes a callback when succesfully connected
        private void ApprovalCheck(byte[] connectionData, ulong clientId, MLAPI.NetworkManager.ConnectionApprovedDelegate callback) {
            //Your logic here
            bool approve = true;
            bool createPlayerObject = true;
            var rotationToSpawnWith = spawn_go.transform.rotation;
            var spawnPosition = spawn_go.transform.position;

            // The prefab hash. Use null to use the default player prefab
            // If using this hash, replace "MyPrefabHashGenerator" with the name of a prefab added to the NetworkPrefabs field of your NetworkManager object in the scene
            ulong? prefabHash = null;

            //If approve is true, the connection gets added. If it's false. The client gets disconnected
            callback(createPlayerObject, prefabHash, approve, spawnPosition, rotationToSpawnWith);
        } 
        #endregion
    }
}