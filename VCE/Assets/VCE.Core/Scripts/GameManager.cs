using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using System;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace VCE.Core {
    public class GameManager : MonoBehaviour {
        public GameObject uiSystem;
        private void Start() {
            ExecuteEvents.Execute<INetworkPanelVisibilityTarget>(uiSystem, null, (x, y) => x.ShowNetworkPanel());
        }
    }
}
