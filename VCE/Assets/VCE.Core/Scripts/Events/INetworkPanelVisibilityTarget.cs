﻿using System;
using UnityEngine.EventSystems;

namespace VCE.Core {
    public interface INetworkPanelVisibilityTarget : IEventSystemHandler{
        public void ShowNetworkPanel();
        public void HideNetworkPanel();
    }
}
