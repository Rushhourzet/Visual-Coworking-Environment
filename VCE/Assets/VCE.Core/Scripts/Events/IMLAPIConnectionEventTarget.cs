﻿using System;
using UnityEngine.EventSystems;

namespace VCE.Core {
    public interface IMLAPIConnectionEventTarget : IEventSystemHandler {
        public void ClientConnectedEvent(string username);
        public void HostedEvent(string username);
        public void ServerHostedEvent();
    }

    public interface IMLAPIDisconnectionEventTarget: IEventSystemHandler {
        public void ClientDisconnectedEvent(string username);
        public void HostedDisconnectedEvent(string username);
        public void ServerClosedEvent();

    }
}
