﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace VCE.Core{
    public interface IPlayerPositionEventTarget : IEventSystemHandler {
        public void LocalPlayerPosition(Vector3 position);
    }
}
