using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace VCE.Core {
    public class Player : NetworkBehaviour {
        public UserConfig userConfig;
        private bool initialized = false;
        private float ms = 4f;

        #region Network Variables - Position and Rotation
        public NetworkVariableVector3 Position = new NetworkVariableVector3(new NetworkVariableSettings {
            WritePermission = NetworkVariablePermission.ServerOnly,
            ReadPermission = NetworkVariablePermission.Everyone
        });
        public NetworkVariableVector3 Rotation = new NetworkVariableVector3(new NetworkVariableSettings {
            WritePermission = NetworkVariablePermission.ServerOnly,
            ReadPermission = NetworkVariablePermission.Everyone
        });
        #endregion

        #region Variables - References
        private GameObject vivox_go;
        private PlayerInputSystem inputSystem;
        public Camera playerCamera;
        public Animator animator;
        int VelocityZHash;
        int VelocityXHash;
        float velocityX;
        float velocityZ;
        #endregion
        #region Variables - UnityEvents
        public UnityEvent cancelButtonEvent;
        public UnityEvent clickButtonEvent;
        #endregion

        #region NetworkStart - Initialize Reference GO's
        public override void NetworkStart() {
            if (inputSystem == null) {
                inputSystem = FindObjectOfType<PlayerInputSystem>();
            }
            if (vivox_go == null) {
                vivox_go = FindObjectOfType<LoginVivox>().gameObject;
            }
            if (playerCamera == null) {
                playerCamera = transform.GetComponentInChildren<Camera>();
            }
            VelocityZHash = Animator.StringToHash("Velocity Z");
            VelocityXHash = Animator.StringToHash("Velocity X");
            initialized = true;
        } 
        #endregion

        #region Update - Handles Input, Movement, Cursor
        void Update() {
            if (this.IsLocalPlayer) {
                if (!playerCamera.enabled) playerCamera.enabled = true;
                //record Input from local player to his networked entity
                if (Cursor.lockState == CursorLockMode.Locked) {
                    InputHandlingServerRpc(inputSystem);
                }
                else {
                    StopGivingInput();
                }
                ExecuteEvents.Execute<IPlayerPositionEventTarget>(vivox_go, null, (x, y) => x.LocalPlayerPosition(Position.Value));
            }
            MovementHandling();
            MovementAnimationHandling();
            //Debug.Log("X: " + velocityX + ", Z: " + velocityZ);

            if (initialized) {
                CursorLockHandling();
            }
        }

        #endregion

        public Player GetLocalPlayer() {
            if (IsLocalPlayer) {
                return this;
            }
            return null;
        }  //unimplemted

        #region Internal Methods

        [ServerRpc] //IMPORTANT For RPC!: Methods with [ServerRpc] or [ClientRpc] need "ServerRpc" or "ClientRpc" in the end of the method name
        private void InputHandlingServerRpc(PlayerInputSystem inputSystem) {
            Position.Value = (Vector3)(inputSystem.movement.PositionMovement * Time.fixedDeltaTime * ms);
            Rotation.Value = (Vector3)(inputSystem.movement.RotationMovement * Time.fixedDeltaTime * userConfig.sens);
        }

        private void StopGivingInput() {
            Position.Value = Vector3.zero;
            Rotation.Value = Vector3.zero;
        }

        private void MovementHandling() {
            transform.Translate(Position.Value);
            transform.Rotate(new Vector3(0f, Rotation.Value.y));
            playerCamera.transform.Rotate(new Vector3(Rotation.Value.x, 0f));
        }

        private void MovementAnimationHandling() {
            float accel = 100f;
            bool movingZ = false;
            bool movingX = false;
            float smol = 0.1f;
            if (Position.Value.z > float.Epsilon || Position.Value.z < -float.Epsilon) {
                movingZ = true;
            }
            if(Position.Value.x > float.Epsilon || Position.Value.x < -float.Epsilon) {
                movingX = true;
            }
            if (movingZ &&( velocityZ < 1.0f && velocityZ > -1.0f)) {
                velocityZ += Position.Value.z * Time.deltaTime *accel;
            }
            if (!movingZ) {
                if (velocityZ > float.Epsilon || velocityZ < -smol) velocityZ /= 1.5f;
            }
            if (movingX && (velocityX < 1.0f && velocityX > -1.0f)) {
                velocityX += Position.Value.x * Time.deltaTime * accel;
            }
            if (!movingX) {
                if(velocityX > float.Epsilon || velocityX < -smol) velocityX /= 2f;
            }
            animator.SetFloat(VelocityZHash, velocityZ);
            animator.SetFloat(VelocityXHash, velocityX);
        }

        private void CursorLockHandling() {
            if (Input.GetButton("Fire1")) {
                Cursor.lockState = CursorLockMode.Locked;
                clickButtonEvent.Invoke();
            }
            if (Input.GetButtonDown("Cancel")) {
                Cursor.lockState = CursorLockMode.None;
                cancelButtonEvent.Invoke();
            }
        }

        #endregion
    }
}