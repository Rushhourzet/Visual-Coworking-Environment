﻿using System;
using UnityEngine;
using VivoxUnity;
using Client = VivoxUnity.Client;
using System.ComponentModel;

//Source: https://www.youtube.com/watch?v=F_Ejzg-m6MM

namespace VCE.Core {
    public class LoginVivox : MonoBehaviour, IMLAPIConnectionEventTarget, IMLAPIDisconnectionEventTarget, IPlayerPositionEventTarget {
        Client client;
        #region Variables - Vivox Data
        private Uri server = new Uri(PersonalVivoxData.API_END_POINT);
        private string issuer = PersonalVivoxData.ISSUER;
        private string domain = PersonalVivoxData.DOMAIN;
        private string tokenKey = PersonalVivoxData.SECRET_KEY;
        private TimeSpan timeSpan = TimeSpan.FromSeconds(90);
        #endregion
        #region Variables - "clipboard"
        private ILoginSession loginSession;
        private IChannelSession channelSession;
        private string channelName;
        private bool channelInited;
        #endregion

        #region Awake - Initialize Client
        private void Awake() {
            client = new Client();
            client.Uninitialize();
            client.Initialize();
            DontDestroyOnLoad(this);
        }
        #endregion

        #region Update - JoinSpatialChannel
        private void Update() {
            if(loginSession != null) {
                if (loginSession.State == LoginState.LoggedIn && !channelInited) {
                    JoinSpatialChannel("Spatial", true, true, true, ChannelType.Positional, new Channel3DProperties());
                    channelInited = true;
                }
            }

        }
        #endregion

        #region OnApplicationQuit - Unitilialize Client
        private void OnApplicationQuit() {
            client.Uninitialize();
        } 
        #endregion

        #region Login Handling
        public void Login(string username) {
            AccountId accountId = new AccountId(issuer, username, domain);
            loginSession = client.GetLoginSession(accountId);
            Bind_Login_Callback_Listeners(true, loginSession);
            loginSession.BeginLogin(server, loginSession.GetLoginToken(tokenKey, timeSpan), ar => { //instead of ar lambda we could pass Login_Result method on bottom aswell
                try {
                    loginSession.EndLogin(ar);
                }
                catch (Exception e) {
                    Bind_Login_Callback_Listeners(false, loginSession);
                    Debug.Log(e.Message);
                    Debug.Log(e.StackTrace);
                }
            });

        }
        public void Logout() {
            loginSession.Logout();
            Bind_Login_Callback_Listeners(false, loginSession);
        }

        public void Login_Status(object sender, PropertyChangedEventArgs loginArgs) {
            var source = (ILoginSession)sender;
            switch (source.State) {
                case LoginState.LoggingIn:
                Debug.Log("Logging in...");
                break;
                case LoginState.LoggingOut:
                Debug.Log("Logging out...");
                break;
            }
        }

        public void Bind_Login_Callback_Listeners(bool bind, ILoginSession loginSession) {
            if (bind) {
                loginSession.PropertyChanged += Login_Status;
            }
            else {
                loginSession.PropertyChanged -= Login_Status;
            }
        }
        #endregion
        #region ChannelHandling
        public void JoinSpatialChannel(string channelName, bool connectAudio, bool connectText, bool switchTransmission, ChannelType channelType, Channel3DProperties channel3DProperties) {
            ChannelId channelId = new ChannelId(issuer, channelName, domain, ChannelType.Positional, channel3DProperties);
            this.channelName = channelName;
            channelSession = loginSession.GetChannelSession(channelId);
            Bind_Channel_Callback_Listeners(true, channelSession);
            channelSession.BeginConnect(connectAudio, connectText, switchTransmission, channelSession.GetConnectToken(tokenKey, timeSpan), ar => {
                try {
                    channelSession.EndConnect(ar);
                }
                catch (Exception e) {
                    Bind_Channel_Callback_Listeners(false, channelSession);
                    Debug.Log(e.Message);
                    Debug.Log(e.StackTrace);
                }
            });
        }
        public void JoinFlatChannel(string channelName, bool connectAudio, bool connectText, bool switchTransmission) {
            ChannelId channelId = new ChannelId(issuer, channelName, domain);
            this.channelName = channelName;
            channelSession = loginSession.GetChannelSession(channelId);
            Bind_Channel_Callback_Listeners(true, channelSession);
            channelSession.BeginConnect(connectAudio, connectText, switchTransmission, channelSession.GetConnectToken(tokenKey, timeSpan), ar => {
                try {
                    channelSession.EndConnect(ar);
                }
                catch (Exception e) {
                    Bind_Channel_Callback_Listeners(false, channelSession);
                    Debug.Log(e.Message);
                    Debug.Log(e.StackTrace);
                }
            });
        }

        public void LeaveChannel(IChannelSession channelToDisconnect) {
            channelToDisconnect.Disconnect();
            loginSession.DeleteChannelSession(new ChannelId(issuer, channelName, domain));
        }

        public void Bind_Channel_Callback_Listeners(bool bind, IChannelSession channelSes) {
            if (bind) {
                channelSes.PropertyChanged += On_Channel_Status_Changed;
            }
            else {
                channelSes.PropertyChanged -= On_Channel_Status_Changed;
            }

        }
        public void On_Channel_Status_Changed(object sender, PropertyChangedEventArgs channelArgs) {
            var source = (IChannelSession)sender;
            switch (source.ChannelState) {
                case ConnectionState.Connecting:
                Debug.Log($"Connecting to {source.Channel.Name}...");
                break;
                case ConnectionState.Connected:
                Debug.Log($"Connected to {source.Channel.Name}");
                break;
                case ConnectionState.Disconnecting:
                Debug.Log($"Disconnecting from {source.Channel.Name}...");
                break;
                case ConnectionState.Disconnected:
                Debug.Log($"Disconnection from {source.Channel.Name}");
                break;
            }
        }

        #endregion

        #region Eventhandling - PlayerPositionEvent
        public void LocalPlayerPosition(Vector3 position) {
            if(channelSession != null) {
                if (channelSession.ChannelState == ConnectionState.Connected) {
                    RefreshPosition(position);
                }
            }
        }
        private void RefreshPosition(Vector3 position) {
            channelSession.Set3DPosition(position, position, Vector3.forward, Vector3.up);
        }

        #endregion
        #region IMLAPIEventHandling

        public void ClientConnectedEvent(string username) {
            Debug.Log("Connecting....");
            Login(username);
        }

        public void HostedEvent(string username) {
            Debug.Log("Connecting....");
            Login(username);
        }

        public void ServerHostedEvent() {
            //do nothing
        }

        public void ClientDisconnectedEvent(string username) {
            Logout();
        }

        public void HostedDisconnectedEvent(string username) {
            Logout();
        }

        public void ServerClosedEvent() {
            Logout();
        }

        #endregion
    }
}


