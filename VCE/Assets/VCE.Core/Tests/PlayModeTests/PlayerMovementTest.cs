using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class PlayerMovementTest
{
    // A Test behaves as an ordinary method
    [Test]
    public void TestWorking()
    {
        Assert.IsTrue(true);
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    [UnityTest]
    public IEnumerator PlayerMovementTest1WithEnumeratorPasses()
    {
        var go = new GameObject();
        go.AddComponent<CharacterController>();
        var originalPosition = go.transform.position;

        yield return new WaitForFixedUpdate();
        Assert.AreNotEqual(originalPosition, go.transform.position);
    }
}
